const express    = require('express')
const properties = require('properties')
const mongoose   = require('mongoose')
const bodyParser = require('body-parser')
const bikes      = require('./routes/bikes');
const users      = require('./routes/users');

const propOptions = { path: true, sections: true, namespaces: true, variables: true }

properties.parse('app.properties', propOptions, (error, props) => {
  mongoose.Promise = global.Promise
  mongoose.connect(props.db.url)
  console.log(`Database ${props.db.name} connected.`)
})

const app  = express()
const port = process.env.PORT || 3000

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use('/api/bikes', bikes)
app.use('/api/users', users)

app.listen(port)
console.log(`Started listening on ${port}.`)
