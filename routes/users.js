const express = require('express')
const User    = require('../models/user')

const router  = new express.Router()

router.route('/')
      .get((req, res) => {
        User.find((err, users) => {
          if (err) res.send(err)

          res.json(users)
          console.log('All users requested via GET.')
        })
      })
      .post((req, res) => {
        const user = new User()
        user.username = req.body.username
        user.password = req.body.password
        user.email = req.body.email
        user.name = req.body.name

        user.save(err => {
          if (err) res.send(err)

          res.json({ message: `User ${user.username} created.` })
          console.log(`User ${user} created via POST.`)
        })
      })

router.route('/:userId')
      .get((req, res) => {
        User.findById(req.params.userId, (err, user) => {
          if (err) res.send(err)

          res.json(user)
          console.log(`User ${user} requested via GET.`)
        })
      })
      .put((req, res) => {
        User.findById(req.params.userId, (err, user) => {
          if (err) res.send(err)

          user.username = req.body.username || user.username
          user.password = req.body.password || user.password
          user.email = req.body.email || user.email
          user.name = req.body.name || user.name

          user.save(err => {
            if (err) res.send(err)
            res.json({ message: `User ${user.username} updated.` })
            console.log(`User ${user} updated via PUT.`)
          })
        })
      })
      .delete((req, res) => {
        User.remove({_id: req.params.userId}, (err, bear) => {
          if (err) res.send(err)

          res.json({ message: `User with id ${req.params.userId} has been removed.` })
          console.log(`User with id ${req.params.userId} has been removed via DELETE.`)
        })
      })

module.exports = router
