const express = require('express')
const Bike    = require('../models/bike')

const router  = new express.Router()

router.route('/')
      .get((req, res) => {
        Bike.find((err, bikes) => {
          if (err) res.send(err)

          res.json(bikes)
          console.log('All bikes requested via GET.')
        })
      })
      .post((req, res) => {
        const bike = new Bike()
        bike.model = req.body.model
        bike.manufacturer = req.body.manufacturer
        bike.year = req.body.year

        bike.save(err => {
          if (err) res.send(err)

          res.json({ message: `Bike ${bike.model} created.`})
          console.log(`Bike ${bike} created via POST.`)
        })
      })

router.route('/:bikeId')
      .get((req, res) => {
        Bike.findById(req.params.bikeId, (err, bike) => {
          if (err) res.send(err)

          res.json(bike)
          console.log(`Bike ${bike} requested via GET.`)
        })
      })
      .put((req, res) => {
        Bike.findById(req.params.bikeId, (err, bike) => {
          if (err) res.send(err)

          bike.model = req.body.model || bike.model
          bike.manufacturer = req.body.manufacturer || bike.manufacturer
          bike.year = req.body.year || bike.year

          bike.save(err => {
            if (err) res.send(err)
            res.json({ message: `Bike ${bike.model} updated.`})
            console.log(`Bike ${bike} updated via PUT.`)
          })
        })
      })
      .delete((req, res) => {
        Bike.remove({_id: req.params.bikeId}, (err, bear) => {
          if (err) res.send(err)

          res.json({ message: `Bike with id ${req.params.bikeId} has been removed.` })
          console.log(`Bike with id ${req.params.bikeId} has been removed via DELETE.`)
        })
      })

module.exports = router
