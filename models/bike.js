const mongoose = require('mongoose')
const Schema   = mongoose.Schema

const BikeSchema = new Schema({
    model        : String,
    manufacturer : String,
    year         : Number
})

module.exports = mongoose.model('Bike', BikeSchema)
